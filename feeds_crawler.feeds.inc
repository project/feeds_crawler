<?php

/**
 * @file
 * Feeds hooks for feeds_crawler.module.
 */

/**
 * Implements hook_feeds_plugings().
 */
function feeds_crawler_feeds_plugins() {
  $path = drupal_get_path('module', 'feeds_crawler') . '/src';
  $info = array();

  $info['FeedsCrawlerBase'] = array(
    'hidden' => TRUE,
    'handler' => array(
      'parent' => 'FeedsHTTPFetcher',
      'class' => 'FeedsCrawlerBase',
      'file' => 'FeedsCrawlerBase.php',
      'path' => $path,
    ),
  );

  $info['FeedsCrawlerNext'] = array(
    'name' => 'Next link fetcher',
    'description' => 'Provides automatic paging of feeds that support a next link.',
    'handler' => array(
      'class' => 'FeedsCrawlerNext',
      'parent' => 'FeedsCrawlerBase',
      'file' => 'FeedsCrawlerNext.php',
      'path' => $path,
    ),
  );

  $info['FeedsCrawlerPattern'] = array(
    'name' => 'Pattern fetcher',
    'description' => 'Provides paging using a URL pattern.',
    'handler' => array(
      'class' => 'FeedsCrawlerPattern',
      'parent' => 'FeedsCrawlerBase',
      'file' => 'FeedsCrawlerPattern.php',
      'path' => $path,
    ),
  );

  return $info;
}
